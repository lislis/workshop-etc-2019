# Put it on a map

See the workshop pad for more info
https://hackmd.okfn.de/etc-2019-putitonamap


## Preparations
- **Text editor**
  - Anything you're already comfortable with! (Vim, Emacs, nano etc)
  - easy entry [Atom](https://atom.io/)
- **Local web server**
    - Python 2: `$ python -m SimpleHTTPServer`
    - Python 3: `$ python -m http.server`
    - if you have Nodejs installed, you can use the package `http-server`; `$ npm i -g http-server && http-server`
- **Get the workshop files**
    - download the zip https://gitlab.com/lislis/workshop-etc-2019/-/archive/master/workshop-etc-2019-master.zip
    - or clone via git `$ git clone https://gitlab.com/lislis/workshop-etc-2019.git`


## Part 1

- start the webserver
- in your browser, open a tab and go to the url that the web server tells you (localhost:8000 for python, :8080 for node)
- You should see something like this

![Screenshot of a map of Athens with 3 markers](screen1.png)

- Open the index.html file in your editor
- What is HTML, CSS, Javascript? Get an overview of the contents


## OpenStreetMap
- The map we're seeing displays information from Open Street Map
- https://www.openstreetmap.org/
- Free and open data through collaboratorive mapping
- Attribution for data and tiles required
- You can edit OSM Data with a browser based editor or with mobile apps, eg [Vespucci](http://vespucci.io/)
- Libraries exist in all major programming languages


### Leaflet.js
- We are using the [leaflet.js](https://leafletjs.com/examples/quick-start/) library to talk to OSM and creatie our interactive map
- There are different map tile providers you can choose from
    - http://maps.stamen.com
    - https://www.thunderforest.com (requires registration)
    - https://www.mapbox.com/gallery/ (requires registration)
- There are different ways to get geo coordinates
    - eg from [OSM](https://openstreetmap.org)
    - or various random [web services](https://www.latlong.net/)

- Play with the coordinates and the tiles!
- Add more markers to the map!
- Change the information in the marker objects, how can you make it appear in the marker popups?

## Part 2

- Git checkout the branch `fetchdata` or (if you downloaded a zip) replace the locations vaiable and foreach loop with this piece of code

``` javascript
window.fetch('epivevaiwmenesparanomespinakides.csv')
    .then((response) => response.text())
    .then((data) => {
        var rows = data.split('\n').slice(1).map(x => x.split(','));

        rows.forEach((x) => {
            var lat =  parseFloat(x[2], 4);
            var lon = parseFloat(x[3], 4);
            if (lat && lon) {
                L.circle([lat, lon], {
                   color: '#ac00e6',
                   fillColor: '#ac00e6',
                   fillOpacity: 0.4,
                   radius: 200
                 })
                    .bindPopup(`<p>${x[5]}, ${x[6]}</p><p>"${x[4]}"</p>`)
                    .addTo(mymap);
               }
             });
           });

```

- Save the file and refresh your browser
- It should look something like this

![Screenshot of a map of Athens with lots of purple cricles](screen2.png)

- Instead of manually adding data, we're *fetching* information from a CSV file
- We're splitting the content by row and then by comma and use index syntax to access the fields with geo information
- For each data point we're drawing a circle that opens a popup with the date and comment in the data point
- Play with the visual representation of the data!

## The dataset and Open Data

- The data set is taken from http://geodata.gov.gr/en/dataset/paranomes-diaphemistikes-pinakides and is distributed under the [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.
- A lot of countries have open data portals
    - http://geodata.gov.gr/en/ (GR)
    - https://www.govdata.de/ (DE)

- If a dataset has geo coordinates, we can put it on our map!
- We can fetch CSV and TSV files easily, split them by new lines and separators
- More advanced data sources will offer JSON or even GeoJSON, a popular data format in web programming. See how to [use GeoJSON with leaflet here](https://leafletjs.com/examples/geojson/)
- Can you find more data sets with geo information?
- Can you add them to the same map with a different visual style?


**The End**


## Further reading
- Countries in the [Open Government Partnership](https://www.opengovpartnership.org/our-members/) agree to open up data
- [This is not an Atlas](https://notanatlas.org), a cool critical mapping project
- [Critical Mapping workshop material](http://orangotango.info/projekte/kollektives-kartieren/handbuch/) (DE, IT, PT)

## FAQ
- Why do we need a local HTTP server? Can't I just open index.html as file?
-> Yes you can and it will probably work. I have had problems in the past with remote dependencies that required a web server to work.Part 2 and window.fetch require a web server anyway, so you might as well start with one. If you intend to only cover Part 1, feel free to skip the web server
